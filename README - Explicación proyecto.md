Se tiene por objetivo crear un código que permita efectuar operaciones básicas de una calculadora(suma, resta, division, multiplicacion).

Para este trabajo se hace uso de la metodología SCRUM, donde cada participante del equipo se encarga de actualizar el código en el repositorio mediante git, según la tarea que le fue asignada.

Se lleva seguimiento del backlog mediante tres tableros: por hacer, en curso y listo.  
